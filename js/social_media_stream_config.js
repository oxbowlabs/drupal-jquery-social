(function($) {
    $(document).ready(function($){
        $('#social-stream').dcSocialStream({
            feeds: {
                twitter: {
                    id: Drupal.settings.social_media_feed.twitterId, //test setting: tinnvec
                    url: Drupal.settings.social_media_feed.pathToModule + '/twitter.php',
                },
                google: {
                    id: Drupal.settings.social_media_feed.googlePlusId, //test setting: 111278225733223821022
                    api_key: Drupal.settings.social_media_feed.googlePlusApiKey, //test setting: AIzaSyBy0X9Jq3s--IaehGrYQ7byXnB_hXzBBjY
                },
                facebook: {
                    id: Drupal.settings.social_media_feed.facebookId, //test setting: 119192121433071
                    text: 'contentSnippet',
                },
                pinterest: {
                    id: Drupal.settings.social_media_feed.pinterestId, //test setting: geek
                },
            },
            days: '720',
            iconPath: Drupal.settings.social_media_feed.pathToModule + '/images/dcsns-dark/',
            imagePath: Drupal.settings.social_media_feed.pathToModule + '/images/dcsns-dark/',
        });
    });
})(jQuery);